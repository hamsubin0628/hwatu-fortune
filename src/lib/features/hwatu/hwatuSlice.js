import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    hwatuList: [
        { month: '1월', name: '소나무' , mean: ['소식', '손님', '남자'], imgUrl: './assets/images/1.png' },
        { month: '2월', name: '매화' , mean: ['애인', '이성'], imgUrl: './assets/images/2.png' },
        { month: '3월', name: '벚꽃' , mean: ['만남', '데이트'], imgUrl: './assets/images/3.png' },
        { month: '4월', name: '등나무' , mean: ['구설수'], imgUrl: './assets/images/4.png' },
        { month: '5월', name: '난초' , mean: ['음식', '국수'], imgUrl: './assets/images/5.png' },
        { month: '6월', name: '모란' , mean: ['길조', '기쁨'], imgUrl: './assets/images/6.png' },
        { month: '7월', name: '싸리' , mean: ['돈', '행운'], imgUrl: './assets/images/7.png' },
        { month: '8월', name: '억새' , mean: ['달밤', '어둠'], imgUrl: './assets/images/8.png' },
        { month: '9월', name: '국화' , mean: ['술'], imgUrl: './assets/images/9.png' },
        { month: '10월', name: '단풍' , mean: ['근심', '걱정'], imgUrl: './assets/images/10.png' },
        { month: '11월', name: '오동' , mean: ['돈', '금전'], imgUrl: './assets/images/11.png' },
        { month: '12월', name: '버드나무' , mean: ['손님'], imgUrl: './assets/images/12.png' }
    ],
    mixCards: [],
    pickCards: []
}

const hwatuSlice = createSlice({
    name: 'hwatu',
    initialState,
    reducers: {
        // 카드 초기화
        resetCard: (state) => {
            state.mixCards = []
            state.pickCards = []
        },
        mixedCards: (state) => {
            // 화투 리스트 사본을 떠서 selected : false 항목을 추가한다
            state.mixCards = [...state.hwatuList].map(card => ({...card, selected: false}))
            // 거꾸로 돌았음 화투 리스트가 0 ~ 11 이기 때문에 => mixCards.length - 1
            for (let i = state.mixCards.length - 1; i > 0; i--) {
                // 카드 랜덤으로 섞기
                const j = Math.floor(Math.random() * (i + 1));
                // 디스트럭처링 a, b = b, a 값 서로 바꿈
                [state.mixCards[i], state.mixCards[j]] = [state.mixCards[j], state.mixCards[i]];
            }
        },
        pickedCard: (state, action) => {
            // 카드 이름이 일치하는 것 찾아오기
            const cardIndex = state.mixCards.findIndex(card => card.name === action.payload);
            // 찾아오는게 있으면 selected = true, pickCards에 cardIndex번째 넣기
            if (cardIndex !== -1) {
                state.mixCards[cardIndex].selected = true;
                state.pickCards.push(state.mixCards[cardIndex]);
            }
        }
    }
})

export const { resetCard, mixedCards, pickedCard } = hwatuSlice.actions
export default hwatuSlice.reducer