'use client'

import {useSelector, useDispatch} from "react-redux";
import {resetCard, mixedCards, pickedCard} from "@/lib/features/hwatu/hwatuSlice";
import axios from "axios";
import {useState} from "react";

export default function Home() {
    const [response, setResponse] = useState('')

    const dispatch = useDispatch()
    const {mixCards, pickCards} = useSelector(state => state.hwatu)

    const handleMixCard = () => {
        dispatch(mixedCards())
    }

    const handlePickCard = (cardName) => {
        if (pickCards.length < 2) {
            dispatch(pickedCard(cardName))
        } else {
            alert('최대 두 장의 카드만 선택할 수 있습니다.')
        }
    }

    const handleReStart = () => {
        dispatch(resetCard())
        setResponse('')
    }

    const handleConfirm = () => {
        if (pickCards.length === 2) {
            const means = pickCards.flatMap(card => card.mean).join(', ')
            const command = `${means} 이라는 단어들을 가지고 5줄 정도의 운세를 만들어서 근엄하게 말해줘`

            const apiUrl = 'https://generativelanguage.googleapis.com/v1/models/gemini-1.5-flash:generateContent?key='

            const data = {"contents": [{"parts": [{"text": command}]}]}
            axios.post(apiUrl, data)
                .then(res => {
                    setResponse(res.data.candidates[0].content.parts[0].text)
                })
                .catch(err => {
                    console.log(err)
                })
        }
    }

    const handleReset = () => {
        dispatch(resetCard())
    }

    return (
        <main className="fn-app">
            <div className="fn-title-section">
                <p className="fn-sub-title">화투로 오늘의 운세를 확인해 보세요.</p>
                <h1 className="fn-title">화투 오늘의 운세</h1>
            </div>
            <div className="fn-content">
                <div className="fn-content-section">
                    {mixCards.length === 0 ? (
                        <div className="fn-start">
                            <button className="fn-btn" onClick={handleMixCard}>화투 패 섞기</button>
                            <img src={`/assets/images/back.png`} className="fn-card-start"/>
                        </div>
                    ) : (
                        <div className="fn-card-align">
                            {/*<p className="fn-choose-card">두 장의 카드를 선택해 주세요.</p>*/}
                            {mixCards.map((card) => (
                                <div key={card.name} onClick={() => handlePickCard(card.name)}>
                                    {card.selected ? (
                                        <div>
                                            <img src={card.imgUrl} className="fn-card"/>
                                            <p className="fn-card-name">{card.name}</p>
                                        </div>
                                    ) : (
                                        <div>
                                            <img src={`/assets/images/back.png`} className="fn-card"/>
                                        </div>
                                    )}
                                </div>
                            ))}
                            {pickCards.length === 2 && (
                                <div className="fn-ai-contents">
                                    <div className="fn-btn-aria">
                                        <button className="fn-bottom-btn" onClick={handleConfirm}>운세보기</button>
                                        <button className="fn-bottom-btn" onClick={handleReStart}>다시하기</button>
                                    </div>
                                    {response && <p className="fn-ai-answer">{response}</p>}
                                </div>
                            )}
                        </div>
                    )}
                </div>
            </div>
        </main>
    )
}
